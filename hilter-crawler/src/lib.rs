#![feature(type_alias_impl_trait)]
use atomic_counter::*;
use futures::future;
use reqwest::{ClientBuilder, Method, Request, Response, Url};
use select::{document::Document, predicate::Name};
use std::collections::{HashMap, HashSet};
use std::time::Duration;
use tower::{BoxError, Service, ServiceBuilder, ServiceExt};

pub struct Config {
    origin: String,
    pub destination: String,
    timeout: u64,
    pub max_depth: u32,
    limit: u64,
}

impl Config {
    pub fn new(
        origin: String,
        destination: String,
        timeout: u64,
        max_depth: u32,
        limit: u64,
    ) -> Self {
        Self {
            origin,
            destination,
            timeout,
            max_depth,
            limit,
        }
    }
}

const WIKI_PREFIX: &str = "https://wikipedia.org";

fn normalize_url(url: &str) -> Option<String> {
    match Url::parse(url) {
        Ok(parsed_url) => parsed_url
            .host_str()
            .filter(|host| host.contains("wikipedia"))
            .and_then(|_| {
                // Absolute url
                let mut split = url.split("wiki");

                for _ in 0..2 {
                    let _ = split.next();
                }

                split
                    .next()
                    .map(|rest| [WIKI_PREFIX, "/wiki", rest].concat())
            }),
        Err(_) => {
            // Relative url
            if url.starts_with("/wiki/") && !url.contains(':') {
                Some([WIKI_PREFIX, url].concat())
            } else {
                None
            }
        }
    }
}

fn extract_links(html: &str) -> HashSet<String> {
    Document::from(html)
        .find(Name("a"))
        .filter_map(|n| n.attr("href"))
        .filter_map(normalize_url)
        .collect()
}

async fn fetch_url(mut client: ServiceClient, url: &str) -> Option<String> {
    let recuest = Request::new(Method::GET, url.parse().ok()?);
    let res = client.ready().await.ok()?.call(recuest).await.ok()?;
    println!("Status for {}: {}", url, res.status());
    res.text().await.ok()
}

async fn find_urls<'a>(
    client: ServiceClient,
    (successful, failed): &(RelaxedCounter, RelaxedCounter),
    new_urls: impl IntoIterator<Item = String>,
) -> Vec<(String, HashSet<String>)> {
    let links = new_urls.into_iter().map(|url| {
        let cloned_cliend = client.clone();
        async move {
            let html = fetch_url(cloned_cliend, &url).await;

            match html {
                Some(_) => successful.inc(),
                None => failed.inc(),
            };

            html.map(|html| extract_links(&html))
                .map(|outgoing| (url, outgoing))
        }
    });

    future::join_all(links)
        .await
        .into_iter()
        .flatten()
        .collect()
}

type ServiceClient = impl Service<Request, Response = Response, Error = BoxError> + Clone;

fn get_service(config: &Config) -> ServiceClient {
    let client = ClientBuilder::new()
        .timeout(Duration::from_secs(config.timeout))
        .build()
        .unwrap();

    ServiceBuilder::new()
        .buffer(100)
        .rate_limit(config.limit, Duration::from_secs(1))
        .service(client)
}

pub async fn main_helper(
    config: Config,
    counters: &(RelaxedCounter, RelaxedCounter),
) -> Option<Vec<String>> {
    let service = get_service(&config);

    let mut previous = HashMap::from([(config.origin.clone(), None::<String>)]);

    let mut visited = HashSet::new();
    let mut new_urls = HashSet::from([config.origin]);
    let mut depth = 1;

    while depth <= config.max_depth {
        visited.extend(new_urls.clone());

        let found_urls = find_urls(service.clone(), counters, new_urls).await;

        for (url, outgoing_links) in &found_urls {
            if outgoing_links.contains(&config.destination) {
                let mut url = Some(url);
                let mut vec = vec![];

                while let Some(inner_url) = url {
                    vec.push(inner_url.to_owned());

                    url = previous.get(inner_url).unwrap().as_ref();
                }
                vec.reverse();

                return Some(vec);
            }

            for outgoing_link in outgoing_links {
                if outgoing_link != url {
                    let _ = previous
                        .entry(outgoing_link.to_owned())
                        .or_insert(Some(url.to_string()));
                }
            }
        }

        let found_urls = found_urls
            .into_iter()
            .flat_map(|(_, value)| value)
            .collect::<HashSet<_>>();

        new_urls = found_urls
            .difference(&visited)
            .map(|s| s.to_owned())
            .collect();

        println!("depth {depth}: found {} new urls", new_urls.len());
        depth += 1;
    }

    None
}
