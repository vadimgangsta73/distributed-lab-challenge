# Long Arithmetic

This crate exports `BigUInt` type represented as a vector of digits.

Common operations are overloaded.

All tests can be run using `cargo test`
