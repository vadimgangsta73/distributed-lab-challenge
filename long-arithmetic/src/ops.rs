use super::helpers::*;
use super::BigUInt;
use itertools::*;
use std::ops::{Add, Mul, Shl, Shr, Sub};

impl Shr<u32> for BigUInt {
    type Output = Self;
    fn shr(self, rhs: u32) -> Self::Output {
        let (shamt64, rem) = (rhs / 64, rhs % 64);
        let tmp = shr_n64(&self, shamt64 as usize);

        shr64(tmp, rem)
    }
}

impl Shl<u32> for BigUInt {
    type Output = Self;
    fn shl(self, rhs: u32) -> Self::Output {
        let (shamt64, rem) = (rhs / 64, rhs % 64);
        let tmp = shl_n64(self, shamt64 as usize);

        shl64(tmp, rem)
    }
}

impl Add for BigUInt {
    type Output = Self;
    fn add(self, rhs: Self) -> Self::Output {
        let (mut inner, carry) = self
            .0
            .iter()
            .zip_longest(rhs.0.iter())
            .map(|x| match x {
                EitherOrBoth::Both(a, b) => (*a, *b),
                EitherOrBoth::Left(a) | EitherOrBoth::Right(a) => (*a, 0),
            })
            .fold((Vec::new(), false), |(mut v, carry), (a, b)| {
                let (tmp, carry) = a.carrying_add(b, carry);

                v.push(tmp);
                (v, carry)
            });

        if carry {
            inner.push(1);
        }

        Self(inner)
    }
}

impl Sub for BigUInt {
    type Output = Self;

    fn sub(self, rhs: Self) -> Self::Output {
        assert!(self >= rhs, "Cannot subtract lower number from bigger");

        let (mut inner, borrow) = self
            .0
            .iter()
            .zip(rhs.0.iter().chain(std::iter::repeat(&0)))
            .fold((Vec::new(), false), |(mut v, borrow), (a, b)| {
                let (tmp, borrow) = a.borrowing_sub(*b, borrow);

                v.push(tmp);
                (v, borrow)
            });

        assert!(!borrow, "Cannot subtract lower number from bigger");

        while inner.last() == Some(&0) {
            let _ = inner.pop();
        }

        Self(inner)
    }
}

impl Mul for BigUInt {
    type Output = Self;

    // quadratic multiplication
    fn mul(self, rhs: Self) -> Self::Output {
        let lhs = self.0.into_iter().enumerate();
        let rhs = rhs.0.into_iter().enumerate();

        lhs.cartesian_product(rhs)
            .map(|((rshift, rhs), (lshift, lhs))| {
                let shamt = rshift + lshift;
                let tmp = mul_helper(lhs, rhs);
                shl_n64(tmp, shamt)
            })
            .fold(Self::from(0), |acc, n| acc + n)
    }
}

#[allow(dead_code)]
pub trait Pow<RHS> {
    type Output;

    fn pow(self, rhs: RHS) -> Self::Output;
}

impl Pow<u64> for BigUInt {
    type Output = Self;

    fn pow(mut self, mut rhs: u64) -> Self::Output {
        if rhs == 0 {
            Self::from(1)
        } else {
            let mut buff = Self::from(1);

            while rhs > 1 {
                if rhs % 2 != 0 {
                    buff *= self.clone();
                    rhs -= 1;
                }
                self *= self.clone();
                rhs /= 2;
            }

            buff * self
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn add_test() {
        assert_eq!(BigUInt::from(0) + 123_456_789, BigUInt::from(123_456_789));
        assert_eq!(BigUInt::from(3) + 123_456_789, BigUInt::from(123_456_792));
        assert_eq!(
            BigUInt::from(u64::MAX) + BigUInt::from(u64::MAX),
            BigUInt::from(vec![u64::MAX - 1, 1])
        );
        assert_eq!(
            BigUInt::from(0) + BigUInt::from(vec![123, 456, 789]),
            BigUInt::from(vec![123, 456, 789])
        );
        assert_eq!(
            BigUInt::from(vec![987, 654, 321]) + BigUInt::from(vec![123, 456, 789]),
            BigUInt::from(vec![1110, 1110, 1110])
        );
        assert_eq!(
            BigUInt::from(vec![u64::MAX, u64::MAX, u64::MAX]) + BigUInt::from(1),
            BigUInt::from(vec![0, 0, 0, 1])
        );
        assert_eq!(
            BigUInt::from(vec![u64::MAX, u64::MAX, u64::MAX]) + BigUInt::from(vec![3, 2, 1]),
            BigUInt::from(vec![2, 2, 1, 1])
        );
    }

    #[test]
    fn sub_test() {
        assert_eq!(
            BigUInt::from(123_456_789) - BigUInt::from(0),
            BigUInt::from(123_456_789)
        );
        assert_eq!(
            BigUInt::from(123_456_789) - BigUInt::from(123_456_789),
            BigUInt::from(0)
        );
        assert_eq!(
            BigUInt::from(vec![123_456_789, 123_456_789]) - BigUInt::from(vec![9, 123_456_789]),
            BigUInt::from(vec![123_456_780])
        );
        assert_eq!(
            BigUInt::from(vec![u64::MAX, u64::MAX, 3]) - BigUInt::from(vec![u64::MAX, u64::MAX, 2]),
            BigUInt::from(vec![0, 0, 1])
        );
        assert_eq!(
            BigUInt::from(vec![0x123, 0x456, 0x789]) - BigUInt::from(vec![0x987, 0x654, 0x321]),
            BigUInt::from(vec![0xFFFF_FFFF_FFFF_F79C, 0xFFFF_FFFF_FFFF_FE01, 0x467])
        );
        assert_eq!(
            BigUInt::from(vec![0, 0, 0, 1]) - BigUInt::from(1),
            BigUInt::from(vec![u64::MAX, u64::MAX, u64::MAX]),
        );
        assert_eq!(
            BigUInt::from(vec![2, 2, 1, 1]) - BigUInt::from(vec![u64::MAX, u64::MAX, u64::MAX]),
            BigUInt::from(vec![3, 2, 1]),
        );
    }

    #[test]
    fn mul_test() {
        assert_eq!(BigUInt::from(0) * 123_456_789, BigUInt::from(0));
        assert_eq!(BigUInt::from(1) * 123_456_789, BigUInt::from(123_456_789));
        assert_eq!(BigUInt::from(3) * 123_456_789, BigUInt::from(370_370_367));
        assert_eq!(
            BigUInt::from(u64::MAX) * BigUInt::from(u64::MAX),
            BigUInt::from(vec![0x0000_0000_0000_0001, 0xFFFF_FFFF_FFFF_FFFE])
        );
        assert_eq!(
            BigUInt::from(vec![0xFFFF_FFFF_FFFF_FFFF, 0x1111_1111_1111_1111, 0x3333])
                * BigUInt::from(vec![0xFFFF_FFFF_FFFF_FFFF, 0x2222_2222_2222_2222, 0x3456]),
            BigUInt::from(vec![
                0x0000_0000_0000_0001,
                0xCCCC_CCCC_CCCC_CCCB,
                0x530E_CA86_41FD_51EC,
                0xCF13_579B_E024_C5E5,
                0xA77_9972
            ])
        );
        assert_eq!(
            BigUInt::from(0x123_0456_0789) * BigUInt::from(0x123_0456_0789),
            BigUInt::from(vec![0xDBA7_EE9B_5844_C751, 0x1_4AD2])
        ); // ** 2
        assert_eq!(
            BigUInt::from(vec![0xDBA7_EE9B_5844_C751, 0x1_4AD2])
                * BigUInt::from(vec![0xDBA7_EE9B_5844_C751, 0x1_4AD2]),
            BigUInt::from(vec![
                0x8601_5398_2E37_07A1,
                0x0F24_B6D5_18CB_E208,
                0x1_AB84_4BFA
            ])
        ); // ** 4
        assert_eq!(
            BigUInt::from(vec![
                0x8601_5398_2E37_07A1,
                0x0F24_B6D5_18CB_E208,
                0x1_AB84_4BFA
            ]) * BigUInt::from(vec![
                0x8601_5398_2E37_07A1,
                0x0F24_B6D5_18CB_E208,
                0x1_AB84_4BFA
            ]),
            BigUInt::from(vec![
                0x33E5_47C2_2368_3341,
                0x438A_893F_691C_BEE9,
                0x1DD8_A227_897B_FE70,
                0x8434_FDB6_53E3_A3DF,
                0xC9F2_99D2_9EF0_90E8,
                0x2
            ])
        ); // ** 8
        assert_eq!(
            BigUInt::from(vec![
                0x33E5_47C2_2368_3341,
                0x438A_893F_691C_BEE9,
                0x1DD8_A227_897B_FE70,
                0x8434_FDB6_53E3_A3DF,
                0xC9F2_99D2_9EF0_90E8,
                0x2
            ]) * BigUInt::from(vec![0xDBA7_EE9B_5844_C751, 0x1_4AD2]),
            BigUInt::from(vec![
                0xCC35_4D9A_2913_BE91,
                0xD87F_0854_504C_4A4C,
                0x078E_81D7_CF7E_C461,
                0x063F_F73B_69F5_D08B,
                0xD396_F1E9_C39B_8D33,
                0xC7E2_8FF2_C990_3B86,
                0x3_9A9E
            ])
        ); // ** 10
    }

    #[test]
    fn pow_test() {
        assert_eq!(BigUInt::from(0x123_0456_0789).pow(0), BigUInt::from(1));
        assert_eq!(
            BigUInt::from(0x123_0456_0789).pow(1),
            BigUInt::from(0x123_0456_0789)
        );
        assert_eq!(
            BigUInt::from(0x123_0456_0789).pow(2),
            BigUInt::from(vec![0xDBA7_EE9B_5844_C751, 0x1_4AD2])
        );
        assert_eq!(
            BigUInt::from(0x123_0456_0789).pow(4),
            BigUInt::from(vec![
                0x8601_5398_2E37_07A1,
                0x0F24_B6D5_18CB_E208,
                0x1_AB84_4BFA
            ])
        );
        assert_eq!(
            BigUInt::from(0x123_0456_0789).pow(8),
            BigUInt::from(vec![
                0x33E5_47C2_2368_3341,
                0x438A_893F_691C_BEE9,
                0x1DD8_A227_897B_FE70,
                0x8434_FDB6_53E3_A3DF,
                0xC9F2_99D2_9EF0_90E8,
                0x2
            ])
        );
        assert_eq!(
            BigUInt::from(0x123_0456_0789).pow(10),
            BigUInt::from(vec![
                0xCC35_4D9A_2913_BE91,
                0xD87F_0854_504C_4A4C,
                0x078E_81D7_CF7E_C461,
                0x063F_F73B_69F5_D08B,
                0xD396_F1E9_C39B_8D33,
                0xC7E2_8FF2_C990_3B86,
                0x3_9A9E
            ])
        );
    }

    #[test]
    fn shl_test() {
        assert_eq!(
            BigUInt::from(0x123_0456_0789) << 10,
            BigUInt::from(0x4_8C11_581E_2400)
        );

        let mut num = BigUInt::from(0x123_0456_0789);
        for _ in 0..125 {
            num <<= 1;
        }
        assert_eq!(
            num,
            BigUInt::from(vec![
                0x0000_0000_0000_0000,
                0x2000_0000_0000_0000,
                0x24_608A_C0F1
            ])
        );

        assert_eq!(
            BigUInt::from(0x123_0456_0789) << 125,
            BigUInt::from(vec![
                0x0000_0000_0000_0000,
                0x2000_0000_0000_0000,
                0x24_608A_C0F1
            ])
        );
    }

    #[test]
    fn shr_test() {
        assert_eq!(
            BigUInt::from(0x4_8C11_581E_2400) >> 10,
            BigUInt::from(0x123_0456_0789),
        );

        let mut num = BigUInt::from(vec![
            0x0000_0000_0000_0000,
            0x2000_0000_0000_0000,
            0x24_608A_C0F1,
        ]);
        for _ in 0..125 {
            num >>= 1;
        }
        assert_eq!(num, BigUInt::from(0x123_0456_0789));

        assert_eq!(
            BigUInt::from(vec![
                0x0000_0000_0000_0000,
                0x2000_0000_0000_0000,
                0x24_608A_C0F1
            ]) >> 125,
            BigUInt::from(0x123_0456_0789),
        );
    }
}
