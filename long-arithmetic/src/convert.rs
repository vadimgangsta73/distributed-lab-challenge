use super::BigUInt;
use std::fmt::Display;
use std::str::FromStr;

impl From<u64> for BigUInt {
    fn from(value: u64) -> Self {
        match value {
            0 => Self(vec![]),
            value => Self(vec![value]),
        }
    }
}

impl From<Vec<u64>> for BigUInt {
    fn from(value: Vec<u64>) -> Self {
        Self(value)
    }
}

impl TryFrom<BigUInt> for u64 {
    type Error = ();

    fn try_from(value: BigUInt) -> Result<Self, Self::Error> {
        match value.0.len() {
            0 => Ok(0),
            1 => Ok(value.0[0]),
            _ => Err(()),
        }
    }
}

impl From<BigUInt> for Vec<u64> {
    fn from(value: BigUInt) -> Self {
        value.0
    }
}

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub enum ParseIntError {
    Empty,
    InvalidDigit,
}

impl FromStr for BigUInt {
    type Err = ParseIntError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        if s.is_empty() {
            return Err(ParseIntError::Empty);
        }

        s.chars().try_fold(Self::from(0), |mut acc, ch| {
            if ch.is_ascii_digit() {
                let n = ch as u64 - u64::from(b'0');
                acc *= 10;
                acc += n;
                Ok(acc)
            } else {
                Err(ParseIntError::InvalidDigit)
            }
        })
    }
}

impl Display for BigUInt {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.is_zero() {
            write!(f, "0")
        } else {
            let mut buff = Vec::new();
            let mut num = self.clone();

            while !num.is_zero() {
                let (div, rem) = num.div_rem(Self::from(10));
                num = div;
                let digit = u64::try_from(rem).unwrap() as u32;
                buff.push(char::from_digit(digit, 10).unwrap());
            }

            write!(f, "{}", buff.into_iter().rev().collect::<String>())
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn big_from_u64_test() {
        assert_eq!(BigUInt::from(0).0, vec![]);
        assert_eq!(BigUInt::from(123_456_789).0, vec![123_456_789]);
    }

    #[test]
    fn parse_test() {
        assert_eq!("0".parse(), Ok(BigUInt::from(0)));
        assert_eq!("123456789".parse(), Ok(BigUInt::from(123_456_789)));
        assert_eq!(
            "12345678910111213141516".parse(),
            Ok(BigUInt::from(vec![0x42B6_5689_328B_BE0C, 0x29D]))
        );
    }

    #[test]
    fn display_test() {
        assert_eq!(BigUInt::from(0).to_string(), "0");
        assert_eq!(BigUInt::from(123_456_789).to_string(), "123456789");
        assert_eq!(
            BigUInt::from(vec![0x42B6_5689_328B_BE0C, 0x29D]).to_string(),
            "12345678910111213141516"
        );
    }
}
