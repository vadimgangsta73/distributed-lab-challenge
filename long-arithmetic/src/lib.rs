#![feature(bigint_helper_methods)]
use std::cmp::{Eq, PartialEq};

mod boilerplate;
mod cmp;
mod convert;
mod helpers;
mod ops;

// Unsigned arbitrary-precision numbers represented in "little-endian"-like way
// 0x1_0000_0000_0000_0000 will be represented like vec![0, 1]
// No leading zeros - 0 is represented by vec![]
#[derive(Eq, PartialEq, Clone, Debug)]
pub struct BigUInt(Vec<u64>);

impl BigUInt {
    #[must_use]
    pub fn is_zero(&self) -> bool {
        self.0.is_empty()
    }
    #[must_use]
    pub fn is_divisible_by_2(&self) -> bool {
        self.0.first().unwrap_or(&0) % 2 == 0
    }
    #[must_use]
    pub fn div_rem(mut self, mut rhs: Self) -> (Self, Self) {
        match (self.is_zero(), rhs.is_zero()) {
            (_, true) => panic!("Division by zero"),

            (true, _) => (Self::from(0), Self::from(0)),
            (_, _) => {
                let self_size = self.bits_num();
                let rhs_size = rhs.bits_num();

                if self_size < rhs_size {
                    // divisor is bigger than dividend
                    (Self::from(0), self)
                } else {
                    let mut ans = Self::from(0);

                    // align divisor and dividend
                    rhs <<= self_size - rhs_size;
                    debug_assert_eq!(self.bits_num(), rhs.bits_num());

                    let mut i = self_size - rhs_size;
                    loop {
                        if self >= rhs {
                            self -= rhs.clone();
                            ans.set_bit(i as usize);
                        }
                        rhs >>= 1;
                        if i == 0 {
                            break;
                        }
                        i -= 1;
                    }

                    (ans, self)
                }
            }
        }
    }
    fn bits_num(&self) -> u32 {
        self.0.last().map_or(0, |last| {
            let rest = 64 - last.leading_zeros();
            (self.0.len() as u32 - 1) * 64 + rest
        })
    }
    fn set_bit(&mut self, n: usize) {
        let (sect, bit) = (n / 64, n % 64);
        if self.0.len() < sect + 1 {
            self.0.extend(vec![0; sect + 1 - self.0.len()]);
        }
        self.0[sect] |= 1 << bit;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn is_zero_test() {
        let num = BigUInt::from(0);
        assert!(num.is_zero());

        let num = BigUInt::from(128);
        assert!(!num.is_zero());

        let num = BigUInt::from(127);
        assert!(!num.is_zero());

        let num = BigUInt::from(vec![0, 0, 0, 123]);
        assert!(!num.is_zero());
    }

    #[test]
    fn divisible_by_2_test() {
        let num = BigUInt::from(0);
        assert!(num.is_divisible_by_2());

        let num = BigUInt::from(12_345_678);
        assert!(num.is_divisible_by_2());

        let num = BigUInt::from(vec![12_345_678, 123, 456, 789]);
        assert!(num.is_divisible_by_2());
    }

    #[test]
    fn bits_num_test() {
        let num = BigUInt::from(0);
        assert_eq!(num.bits_num(), 0);

        let num = BigUInt::from(128);
        assert_eq!(num.bits_num(), 8);

        let num = BigUInt::from(127);
        assert_eq!(num.bits_num(), 7);

        let num = BigUInt::from(vec![0, 0, 0, 123]);
        assert_eq!(num.bits_num(), 64 * 3 + 7);
    }

    #[test]
    fn set_bit_test() {
        let mut num = BigUInt::from(0);
        num.set_bit(130);
        assert_eq!(num, BigUInt::from(vec![0, 0, 4]));

        let mut num = BigUInt::from(128);
        num.set_bit(130);
        assert_eq!(num, BigUInt::from(vec![128, 0, 4]));

        let mut num = BigUInt::from(vec![128, 0, 9]);
        num.set_bit(130);
        assert_eq!(num, BigUInt::from(vec![128, 0, 13]));

        let mut num = BigUInt::from(vec![128, 0, 8]);
        num.set_bit(131);
        assert_eq!(num, BigUInt::from(vec![128, 0, 8]));

        let mut num = BigUInt::from(128);
        num.set_bit(4);
        assert_eq!(num, BigUInt::from(144));

        let mut num = BigUInt::from(0);
        num.set_bit(0);
        assert_eq!(num, BigUInt::from(1));

        let mut num = BigUInt::from(0);
        num.set_bit(1);
        assert_eq!(num, BigUInt::from(2));
    }

    #[test]
    fn div_rem_test() {
        let num = BigUInt::from(0);
        let (div, rem) = num.div_rem(BigUInt::from(1));
        assert_eq!(div, BigUInt::from(0));
        assert_eq!(rem, BigUInt::from(0));

        let num = BigUInt::from(127);
        let (div, rem) = num.div_rem(BigUInt::from(2));
        assert_eq!(div, BigUInt::from(63));
        assert_eq!(rem, BigUInt::from(1));

        // let num = BigUInt::from(122);
        // let (div, rem) = num.div_rem(BigUInt::from(2));
        // assert_eq!(div, BigUInt::from(63));
        // assert_eq!(rem, BigUInt::from(1));
    }
}
